# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Stock Market
* Key functions (add/delete)
    1. init()
    2. selectram()
    3. buything()
    3. sellthing()
    4. Release()
    5. notifyMe()
    
* Other functions (add/delete)
    1. delItem()
    2. create_alert()
    3. ssort()
    4. random() 、 getRandom()

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：https://midterm-65d7c.firebaseapp.com

# Components Description : 
1. init : 將所有資料初始化(由於新帳戶沒有錢，所以會建個balance給他存在Realtime Database)
2. selectram : 這次project最難的部分，這是對股票的價格做浮動，但是如果用setInterval一直跑，在多個本地端會同時修改database
導致問題產生，所以用 .on detect setTimeout，只能等待有人上架商品，購買，販賣等等就會做浮動。
3. buything 、 sellthing 、 Release : 這部分全部都在處理購買販賣以及上架，只有純粹對database做修訂，html的部分則由 renew()處理
比較麻煩的點在於把資料拉下來後不能跟其他人衝突，所以要存放在local，只是還是有可能資料不對等。

# Other Functions Description(1~10%) : 
1. ssort : 對html的stocks做排版， higher lower 是處理浮動率的。
2. delItem : 讓自己上架的商品售出後不再出現在stock裡面。

## Security Report (Optional)
有處理掉字串、處理掉數字過大或過小。