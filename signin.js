function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert" and clean input field
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(function () {
            window.location.assign('index.html');
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode= error.code;
            var errorMessage= error.message;
            create_alert("error", errorMessage);
        });
    });

    btnGoogle.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
// This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
// The signed-in user info.
            var user = result.user;
            console.log('success');
            window.location.assign('index.html');
// ...
        }).catch(function(error) {
// Handle Errors here.
            var errorCode= error.code;
            var errorMessage= error.message;
// The email of the user's account used.
            var email = error.email;
// The firebase.auth.AuthCredentialtype that was used.
            var credential = error.credential;
            create_alert("error", errorMessage);
        });
    });

    btnSignUp.addEventListener('click', function () {
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email,password).then(function () {
            create_alert("success", "");
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode= error.code;
            var errorMessage= error.message;
            create_alert("error", errorMessage);
        });
    });
}


// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        console.log("success");
        str_html = '<div class="w3-panel w3-pale-green w3-display-container w3-round-xxlarge">'+
            '<span onclick="this.parentElement.style.display=\'none\'"\n' +
            '  class="w3-button w3-round-xxlarge w3-right" >&times;</span>'+
            '<h3>Success!</h3>'+
            '<p>'+ message + '</p>'+
            '</div>';
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        console.log("error");
        str_html = '<div class="w3-panel w3-pale-red w3-display-container w3-round-xxlarge">'+
            '<span onclick="this.parentElement.style.display=\'none\'"\n' +
            '  class="w3-button w3-round-xxlarge w3-right" >&times;</span>'+
            '<h3>Error!</h3>'+
            '<p>'+ message + '</p>'+
            '</div>';
        alertarea.innerHTML = str_html;
    }
}



window.onload = function () {
    initApp();
};