function w3_open() {
    document.getElementById("mySidebar").style.display = "block";
}

function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
}


var userid;
const maxnum = 15;
const minnum = -15;

function resetall() {
    var elements = document.getElementById('merchandize');
    var stockRef = firebase.database().ref('stock_list');
    var str_stock = "<div class=\"stock w3-bar-item\" style=\"width: 20%;min-width:250px\">";
    var str_sells = "<div class=\"sells w3-bar-item\" style=\"width: 20%\">";
    var str_price = "<div class=\"price w3-bar-item\" style=\"width: 20% \" id=";
    var str_chr = "<div class=\"ch w3-bar-item\" style=\"width: 20%; color:red\" id=";
    var str_chg = "<div class=\"ch w3-bar-item\" style=\"width: 20%; color:green\" id=";
    var input_id = "<input class=\"w3-bar-item w3-border\" type=\"number\" min=\"1\" max=\"1000\" style=\"width: 80px\" value=\"1\" id=";
    var buy_div = "<div class=\"buy w3-bar-item w3-button w3-pale-green\" align=\"center\" style=\"width: 12%\" onclick=\"buything(id)\" id=";
    var input_sells = "<input type=\"hidden\" value=";
    var input_price = "<input type=\"hidden\" value=";
    var input_chNet = "<input type=\"hidden\" value=";
    var input_userid = "<input type=\"hidden\" value=";

    stockRef.once('value')
        .then(function (snapshot) {
            var obj = snapshot.val();
            var stock = Object.values(obj);
            var stock_id0 = stock[0].id;
            var str_ch;
            if(stock[0].chNet > 1)
                str_ch = str_chr;
            else
                str_ch = str_chg;
            elements.innerHTML =
                '<div class="w3-panel">'+
                    str_stock + stock[0].Fullname + '</div>'+
                    str_sells + stock[0].sells + '</div>'+
                    str_price + stock_id0 + '_pshow>' + stock[0].price + '</div>'+
                    str_ch + stock_id0 + '_chshow>' + stock[0].chNet + '</div>'+
                    input_id + stock_id0 + '>'+
                    buy_div + stock_id0 + '>Buy</div>'+
                    input_sells + stock[0].sells + " id=" + stock_id0 +'_sells>'+
                    input_price + stock[0].price + " id=" + stock_id0 +'_price>'+
                    input_chNet + stock[0].chNet + " id=" + stock_id0 +'_chNet>'+
                    input_userid + userid + " id=" + stock_id0 + "__>"+
                '</div>';
            for (let i = 1 ; i<stock.length ; i++ ){
                var stock_id = stock[i].id;
                if(stock[i].chNet > 1)
                    str_ch = str_chr;
                else
                    str_ch = str_chg;
                if(stock[i].id == userid){
                    elements.innerHTML +=
                        '<div class="w3-panel">'+
                        str_stock + stock[i].Fullname + '</div>'+
                        str_sells + stock[i].sells + '</div>'+
                        str_price + stock_id + '_pshow>' + stock[i].price + '</div>'+
                        str_ch + stock_id + '_chshow>' + stock[i].chNet  + '</div>'+
                        // input_id + stock_id + '>'+
                        input_sells + stock[i].sells + " id=" + stock_id +'_sells>'+
                        input_price + stock[i].price + " id=" + stock_id +'_price>'+
                        input_chNet + stock[i].chNet + " id=" + stock_id +'_chNet>'+
                        input_userid + userid + " id=" + stock_id + "__>"+
                        '</div>';
                } else {
                    elements.innerHTML +=
                        '<div class="w3-panel">' +
                        str_stock + stock[i].Fullname + '</div>' +
                        str_sells + stock[i].sells + '</div>' +
                        str_price + stock_id + '_pshow>' + stock[i].price + '</div>' +
                        str_ch + stock_id + '_chshow>' + stock[i].chNet + '</div>' +
                        input_id + stock_id + '>' +
                        buy_div + stock_id + '>Buy</div>' +
                        input_sells + stock[i].sells + " id=" + stock_id + '_sells>' +
                        input_price + stock[i].price + " id=" + stock_id + '_price>' +
                        input_chNet + stock[i].chNet + " id=" + stock_id + '_chNet>' +
                        input_userid + userid + " id=" + stock_id + "__>" +
                        '</div>';
                }
            }

        })
        .catch(
            e => window.location.assign('signin.html')
        );
}

function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var id = document.getElementById('dynamic-id');

        // Check user login
        if (user) {
            user_email = user.email;
            userid = user_email.split("@")[0];
            menu.innerHTML ="<div class=\"w3-bar-item w3-button fas fa-id-badge\" onclick=\"window.location.assign('account.html');\">"+
                "Account"+
                "</div>"+
                "<a href=\"signin.html\" class=\"w3-bar-item w3-button\" id=\"logout-btn\">"+
                "Logout"+
                "</a>";
            id.innerHTML = "<div align=\"right\">ID : "+
                userid
                +"</div>\n" ;
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var logout = document.getElementById('logout-btn');
            logout.addEventListener('click',function () {
                firebase.auth().signOut().then(function() {
                    // Sign-out successful.
                    create_alert('success', 'Successfully log out');
                    console.log('success');
                }).catch(function(error) {
                    // An error happened.
                    create_alert('error', 'Something wrong');
                    console.log('error '+error.message);
                });
            });

            var userID = userid;
            var database = firebase.database();
            database.ref("users/" + userID).update(
                {
                    bankrupt: -1
                }
            );

            var userRef = firebase.database().ref('users/' + userID);
            userRef.once('value').then(function (snapshot) {
                var obj = snapshot.val();
                var detail = Object.values(obj);
                var balance = document.getElementById('balance');
                if(detail.length == 1){
                    database.ref("users/" + userID).update({
                        balance: 1000
                    });
                    balance.innerHTML = "Balance : 1000";
                } else {
                    balance.innerHTML = "Balance : "+ detail[detail.length-2];
                }
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a href=\"signin.html\" class=\"w3-bar-item w3-button\"> Login</a>";
            id.innerHTML = "<div align=\"right\">ID : None</div>";
            // document.getElementById('post_list').innerHTML = "";
        }
    });
    selectram();
}

var random = setInterval(renew, 1000);

function getRandom(min,max){
    return Math.floor(Math.random()*(max-min+1))+min;
}

function renew() {
    var stockRef = firebase.database().ref('stock_list');
    stockRef.once('value').then( async function (snapshot) {
        var obj = snapshot.val();
        var stock = Object.values(obj);
        for (let i = 0 ; i<stock.length ; i++ ) {
            var htmlprice = document.getElementById(stock[i].id + "_pshow");
            var htmlchNet = document.getElementById(stock[i].id + "_chshow");
            var _price = document.getElementById(stock[i].id + '_price');
            if(htmlprice != null && htmlchNet != null) {
                htmlprice.innerHTML = stock[i].price;
                htmlchNet.innerHTML = stock[i].chNet;
                var newNet = parseFloat(stock[i].chNet);
                _price.setAttribute('value', parseInt(stock[i].price));
                if (newNet > 1)
                    htmlchNet.setAttribute('style', 'width:20%;' + ' color:red');
                else if (newNet < 1)
                    htmlchNet.setAttribute('style', 'width:20%;' + ' color:green');
            }
        }
    });
    // console.log('renew');
}

function selectram() {
    var stockRef = firebase.database().ref('stock_list');
    stockRef.once('value').then( function (snapshot) {
        setTimeout( function () {
            var obj = snapshot.val();
            var keys = Object.keys(obj);
            var stock = Object.values(obj);
            for (let i = 0 ; i<stock.length ; i++ ){
                // var htmlprice = document.getElementById(stock[i].id + "_pshow");
                // var htmlchNet = document.getElementById(stock[i].id + "_chshow");
                // var _price = document.getElementById(stock[i].id+'_price');

                var oldprice = parseInt(stock[i].price);
                var newprice = parseInt((oldprice + getRandom(minnum, maxnum)));
                if (newprice < 0) {
                    newprice = (-newprice) * 3;
                } else if (newprice > 1000) {
                    newprice = newprice * 0.7329;
                }
                var newNet = (newprice / oldprice).toFixed(3);
                var index = i;
                var database = firebase.database();
                if (newNet > 1.25) {
                    notifyMe();
                }
                stock[index].price = newprice;
                stock[index].chNet = newNet;
                database.ref("stock_list/" + keys[index]).update(
                    {
                        id: stock[index].id,
                        Fullname: stock[index].Fullname,
                        sells: stock[index].sells,
                        price: stock[index].price,
                        chNet: stock[index].chNet
                    }
                );

            }
        }, 10000);

        });
}


var option = {
    icon: 'https://img.icons8.com/dusk/64/000000/google-alerts.png',
    body: 'The Net Change Rate > 1.25 show up. Go buy!'
};

document.addEventListener('DOMContentLoaded', function () {
    if (!Notification) {
        alert('Desktop notifications not available in your browser. Try Chromium.');
        return;
    }

    if (Notification.permission !== "granted")
        Notification.requestPermission();
});

function notifyMe() {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
    else {
        var notification = new Notification('Notice!', option);

    }

}


// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        console.log("success");
        str_html = '<div class="w3-panel w3-pale-green w3-display-container w3-round-xxlarge">'+
            '<span onclick="this.parentElement.style.display=\'none\'"\n' +
            '  class="w3-button w3-round-xxlarge w3-right" >&times;</span>'+
            '<h3>Success!</h3>'+
            '<p>'+ message + '</p>'+
            '</div>';
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        console.log("error");
        str_html = '<div class="w3-panel w3-pale-red w3-display-container w3-round-xxlarge">'+
            '<span onclick="this.parentElement.style.display=\'none\'"\n' +
            '  class="w3-button w3-round-xxlarge w3-right" >&times;</span>'+
            '<h3>Error!</h3>'+
            '<p>'+ message + '</p>'+
            '</div>';
        alertarea.innerHTML = str_html;
    }
}


window.onload = function () {
    resetall();
    init();
};

////////////////////////////////////////////////////////////////////////////////////////////////
// var stockRef = firebase.database();
// var total_stock = document.getElementsByClassName('buy');
//
// for (let i = 0 ; i < total_stock.length ; i++) {
//     var stock_id = total_stock[i].id;
//     var stock_fullname = document.getElementsByClassName('stock')[i].innerHTML;
//     var stock_sells = document.getElementById(stock_id+'_sells');
//     var stock_price = document.getElementById(stock_id+'_price');
//     var stock_chNet = document.getElementById(stock_id+'_chNet');
//     console.log(i,stock_id);
//     stockRef.ref("stock_list/").push().set(
//         {
//             id: stock_id,
//             Fullname: stock_fullname,
//             sells: stock_sells.value,
//             price: stock_price.value,
//             chNet: stock_chNet.value
//         }
//     );
// }

// var buy_btn = document.getElementsByClassName('buy');
//
// for(var i = 0 ; i < buy_btn.length ; i++) {
//     buy_btn[i].addEventListener('click', function () {
//         var _stock = this.id;
//         var _count = document.getElementById('_' + _stock).value;
//         // var user = firebase.database().ref("users/" + user_email);
//         if (user_email != "") {
//             /// TODO 6: Push the post to database's "com_list" node
//             ///         1. Get the reference of "com_list"
//             ///         2. Push user email and post data
//             ///         3. Clear text field
//             var userID = user_email.split("@");
//             var database = firebase.database();
//             database.ref("users/" + userID[0]).push().set(
//                 {
//                     name: _stock,
//                     quantity: _count
//                 }
//             );
//             create_alert('success','Thanks for purchase! You buy : ' + _stock + ' * ' + _count);
//         } else {
//             create_alert('error','Please login or wait a moment');
//         }
//     }, false);
// }
