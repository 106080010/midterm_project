function buything (id) {

    var userid = document.getElementById(id+'__').value;
    var database = firebase.database();
    var userRef = firebase.database().ref('users/' + userid);
    var _stock = id;
    var _count = document.getElementById(id).value;
    var _price = document.getElementById(id+"_price").value;

    console.log(_count);
    console.log(_price);

    userRef.once('value').then(function (snapshot) {
        if (userid != "") {
            var obj = snapshot.val();
            var detail = Object.values(obj);
            var balance = detail[detail.length-2];
            if( isNaN(_price)){
                create_alert('error','Please enter correct number');
            }
            else if(_price*_count > balance){
                create_alert('error','You will gotta bankrupt');
                return;
            }
            balance -= _price*_count;
            database.ref("users/" + userid).update({
                balance: balance
            });
            var Balance = document.getElementById('balance');
            Balance.innerHTML = "Balance : "+ balance;

            var exist = false;
            var i;
            for (i = 0 ; i < detail.length-2 ; i++) {
                if(detail[i].id == _stock){
                    exist = true;
                    break;
                }
            }
            if(exist){
                database.ref("users/" + userid + "/" + _stock.toUpperCase()).update(
                    {
                        id: _stock,
                        quantity: parseInt(_count) + parseInt(detail[i].quantity)
                    }
                );
            } else {
                database.ref("users/" + userid + "/" + _stock.toUpperCase()).update(
                    {
                        id: _stock,
                        quantity: _count
                    }
                );
            }

            create_alert('success','Thanks for purchase! You buy : ' + _stock + ' * ' + _count);

            var stockRef = firebase.database().ref('stock_list');
            stockRef.once('value').then(function (snapshot) {
                var obj = snapshot.val();
                var keys = Object.keys(obj);
                var stock = Object.values(obj);

                var index;
                for (index = 0 ; index<stock.length ; index++){
                    if(stock[index].id == _stock){
                        break;
                    }
                }

                stock[index].sells =  parseInt(stock[index].sells) + parseInt(_count);

                database.ref("stock_list/" + keys[index]).update(
                    {
                        id: stock[index].id,
                        Fullname: stock[index].Fullname,
                        sells: stock[index].sells,
                        price: stock[index].price,
                        chNet: stock[index].chNet
                    }
                );
                location.reload();
            });
        } else {
            create_alert('error','Please login or wait a moment');
        }
    });

    ///////////////////////////////////////
    // var user = firebase.database().ref("users/" + user_email);
}

function sellthing(id) {

    var userid = document.getElementById(id+'__').value;
    var database = firebase.database();
    var userRef = firebase.database().ref('users/' + userid);
    var _stock = id;
    var _count = document.getElementById(id).value;
    var _max = document.getElementById(id).max;
    var _price = document.getElementById(id+"_price").value;

    console.log(_count);
    console.log(_stock);
    console.log(_price);

    if( isNaN(_count)){
        create_alert('error','Please enter correct number');
    }
    else if(_max < _count){
        create_alert('error','Too much!!!');
        return;
    }

    userRef.once('value').then(function (snapshot) {
        if (userid != "") {
            var obj = snapshot.val();
            var detail = Object.values(obj);
            var balance = detail[detail.length-2];
            balance += _price*_count;
            database.ref("users/" + userid).update({
                balance: balance
            });
            var Balance = document.getElementById('balance');
            Balance.innerHTML = "Balance : "+ balance;

            var i;
            for (i = 0 ; i < detail.length-2 ; i++) {
                if(detail[i].id == _stock){
                    break;
                }
            }
            if(detail[i].quantity == _count){
                database.ref("users/" + userid + "/" + _stock.toUpperCase()).remove();
            } else {
                database.ref("users/" + userid + "/" + _stock.toUpperCase()).update(
                    {
                        id: _stock,
                        quantity: parseInt(detail[i].quantity) - parseInt(_count)
                    }
                );
            }
            location.reload();
            create_alert('success','You got ! : ' + _price*_count + ' USD');
        } else {
            create_alert('error','Please login or wait a moment');
        }
    });

}

function Release() {
    var userid = document.getElementById('release').value;
    var stock_fullname = document.getElementById('stockname').value;
    var stock_price = document.getElementById('stockprice').value;
    // var stock_quantity = document.getElementById('stocknumber').value;
    if(isNaN(stock_price)){
        create_alert('error', 'Please enter correct type');
        return;
    } else if(parseInt(stock_price) > 100 || parseInt(stock_price) < 10 ){
        create_alert('error', 'Please enter acceptable quantity');
        return;
    }
    console.log(stock_fullname[0] !== stock_fullname[0].toUpperCase());
    if(stock_fullname[0] !== stock_fullname[0].toUpperCase()) {
        create_alert('error', 'Please enter acceptable type');
        return;
    }
    else if(stock_fullname.search("<") >= 0 || stock_fullname.search(">") >= 0 || stock_fullname.search('/') >= 0){
        create_alert('error', 'Please enter acceptable type');
        return;
    }

    var userRef = firebase.database().ref('users/' + userid);
    userRef.once('value').then( function (snapshot) {
        var user = snapshot.val();
        console.log(parseInt(stock_price));
        if(user.balance < parseInt(stock_price)){
            create_alert('error', 'Sorry, you don\'t have enough money');
            return;
        } else {
            userRef.update({
                balance: user.balance - parseInt(stock_price)
            });
            var database = firebase.database();
            var stockRef = firebase.database().ref('stock_list/' + userid);
            stockRef.once('value').then( function (snapshot) {
                var obj = snapshot.val();
                if (obj == null){
                    database.ref('stock_list/'+userid.toUpperCase()).update(
                        {
                            id: userid,
                            Fullname: stock_fullname,
                            sells: 1,
                            price: stock_price,
                            chNet: 0
                        }
                    );
                    create_alert('success','The stock is released!');
                    location.reload();
                } else {
                    create_alert('error','Only accept one stock on the market');
                }
                // console.log(obj);
            });
        }
    });
}

function delItem(id){
    var stock = firebase.database().ref('stock_list/' + id.toUpperCase());
    var userRef = firebase.database().ref('users/' + userid);
    var usersRef = firebase.database().ref('users');
    var stock_price = document.getElementById(id+'_pt').innerHTML;
    userRef.once('value').then( function (snapshot) {
        var user = snapshot.val();
        usersRef.once('value').then(function (event) {
            var users = event.val();
            var keys = Object.keys(users);
            users = Object.values(users);
            console.log(users);
            for(let i = 0 ; i<users.length ; i++){
                if(keys[i] != userid){
                    var content = id.toUpperCase();
                    firebase.database().ref('users/' + keys[i] +'/' + content).remove();
                }
            }
        });
        // console.log(user.balance, stock_price);
        userRef.update({
            balance: (user.balance + parseInt(stock_price))
        });
        stock.remove();
        location.reload();
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        console.log("success");
        str_html = '<div class="w3-panel w3-pale-green w3-display-container w3-round-xxlarge">'+
            '<span onclick="this.parentElement.style.display=\'none\'"\n' +
            '  class="w3-button w3-round-xxlarge w3-right" >&times;</span>'+
            '<h3>Success!</h3>'+
            '<p>'+ message + '</p>'+
            '</div>';
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        console.log("error");
        str_html = '<div class="w3-panel w3-pale-red w3-display-container w3-round-xxlarge">'+
            '<span onclick="this.parentElement.style.display=\'none\'"\n' +
            '  class="w3-button w3-round-xxlarge w3-right" >&times;</span>'+
            '<h3>Error!</h3>'+
            '<p>'+ message + '</p>'+
            '</div>';
        alertarea.innerHTML = str_html;
    }
}