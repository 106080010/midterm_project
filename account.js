
function w3_open() {
    document.getElementById("mySidebar").style.display = "block";
}

function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
}
var userid;
var amount = [];
var real_stock = null;
const maxnum = 15;
const minnum = -15;

function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var id = document.getElementById('dynamic-id');

        // Check user login
        if (user) {
            user_email = user.email;
            userid = user_email.split("@")[0];
            menu.innerHTML ="<div class=\"w3-bar-item w3-button fas fa-id-badge\">"+
                "Account"+
                "</div>"+
                "<a href=\"signin.html\" class=\"w3-bar-item w3-button\" id=\"logout-btn\">"+
                "Logout"+
                "</a>";
            id.innerHTML = "<div align=\"right\">ID : "+
                userid
                +"</div>\n" ;
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var logout = document.getElementById('logout-btn');
            logout.addEventListener('click',function () {
                firebase.auth().signOut().then(function() {
                    // Sign-out successful.
                    create_alert('success', 'Successfully log out');
                    console.log('success');
                }).catch(function(error) {
                    // An error happened.
                    create_alert('error', 'Something wrong');
                    console.log('error '+error.message);
                });
            });
/////////////////////////////////////////////////////////////////////////////////////////////////
            var userID = userid;
            var database = firebase.database();
            database.ref("users/" + userID).update(
                {
                    bankrupt: -1
                }
            );

////////////////////////////////////////////////////////////////////////////////////////////////
            var userRef = firebase.database().ref('users/' + userID);
            userRef.once('value').then(function (snapshot) {
                console.log('a');
                var obj = snapshot.val();
                var keys = Object.keys(obj);
                var detail = Object.values(obj);
                // console.log("detail : ",detail);
                var balance = document.getElementById('balance');
                var check = document.getElementById('loading');
                var input_userid_r = "<input type=\"hidden\" id =\"release\"  value=";
                var elements = document.getElementById('merchandize');
                var stockRef = firebase.database().ref('stock_list');
                var str_stock = "<div class=\"w3-bar-item\" style=\"word-break : normal;width: 18%\">";
                var str_quantity = "<div class=\"w3-bar-item\" style=\"width: 18%\">";
                var str_total = "<div class=\"w3-bar-item\" style=\"width: 18%\" id=";
                var str_single = "<div class=\"w3-bar-item\" style=\"width: 18%\" id=";
                var str_chr = "<div class=\"ch w3-bar-item\" style=\"width: 13%; color:red\" id=";
                var str_chg = "<div class=\"ch w3-bar-item\" style=\"width: 13%; color:green\" id=";
                // var str_ch = "<div class=\"w3-bar-item\" style=\"width: 13%\">";
                var sell_div = "<div class=\"w3-bar-item w3-button w3-pale-red\" onclick=\"sellthing(id)\" id=";
                var sell_div_self = "<div class=\"w3-bar-item w3-button w3-pale-red\" onclick=\"delItem(id)\" id=";
                var input_quantity = "<input class=\"w3-bar-item \" type=\"number\" min=\"1\" max=";
                var input_price = "<input type=\"hidden\" value=";
                var input_userid = "<input type=\"hidden\"  value=";
                var sell_type_h = "<div class=\"w3-dropdown-hover\">\n" +
                    "                    <div class=\"w3-bar-item w3-button w3-pale-red\" style=\"width: 120px\">sell</div>\n" +
                    "                    <div class=\"w3-dropdown-content w3-bar-block w3-border\">\n" ;
                var sell_type_t = "sell</div>\n" +
                    "                    </div>\n" +
                    "                </div>";
                if(detail.length == 1){
                    database.ref("users/" + userID).update({
                        balance: 1000
                    });
                    balance.innerHTML = "Balance : 1000";
                }
                balance.innerHTML = "Balance : "+ detail[detail.length-2] + input_userid_r + userid + '>';
                if(detail.length == 2){
                    check.innerHTML =
                        "<div class=\"w3-panel w3-light-green w3-card-4\">\n" +
                        "  <p>Go buy something!</p>\n" +
                        "</div>";
                    firebase.database().ref('stock_list').once('value').then(function (snapshot) {
                        var obj = snapshot.val();
                        var stock = Object.values(obj);
                        var str_ch;
                        // console.log("stock : ",real_stock);
                        for (let j = 0 ; j < stock.length ; j++){
                            // console.log(stock[j].id, userid);
                            if(stock[j].id == userid){
                                elements.innerHTML = "";
                                if(stock[j].chNet > 1)
                                    str_ch = str_chr;
                                else
                                    str_ch = str_chg;
                                var sellbtn = "<div class=\"w3-bar-item w3-button w3-red\" align=\"center\" style=\"width: 10%\" onclick=\"sellthing(id)\" id=";
                                elements.innerHTML =
                                    '<div class="w3-panel w3-pale-green">'+
                                    str_stock + stock[j].id + '</div>'+
                                    str_quantity + stock[j].sells + '</div>'+

                                    str_total + stock[j].id + '_pt>' + stock[j].price * stock[j].sells + '</div>'+
                                    str_single + stock[j].id + '_ps>' + stock[j].price + '</div>'+
                                    str_ch + stock[j].id + '_chshow>' + stock[j].chNet + '</div>'+
                                    // deletebtn + stock[j].id + '>' + 'Delete' + '</div>' +
                                    sell_type_h +
                                    input_quantity + stock[j].sells + " value=\"1\" id=" + stock[j].id + '>' +
                                    sell_div_self + stock[j].id + '>' +
                                    sell_type_t +
                                    input_price + stock[j].price + " id=" + stock[j].id +'_price>'+
                                    input_userid + userid + " id=" + stock[j].id + "__>"+
                                    '</div>';
                                // console.log('reach');
                            }
                        }
                    })
                } else {

                    stockRef.once('value').then(function (snapshot) {
                            var obj = snapshot.val();
                            var stock = Object.values(obj);
                            for (let i = 0; i < detail.length; i++) {
                                for (let j = 0; j < stock.length; j++) {
                                    if(real_stock == null)
                                        real_stock = [];
                                    if (stock[j].id == detail[i].id) {
                                        real_stock[i] = stock[j];
                                    }
                                }
                            }

                            var str_ch;
                            console.log("stock : ",real_stock);
                            console.log("detail : ",detail);
                            console.log("stock2 : ",stock);
                            for (let j = 0 ; j < stock.length ; j++){
                                // console.log(stock[j].id, userid);
                                if(stock[j].id == userid){
                                    elements.innerHTML = "";
                                    if(stock[j].chNet > 1)
                                        str_ch = str_chr;
                                    else
                                        str_ch = str_chg;
                                    elements.innerHTML =
                                        '<div class="w3-panel w3-pale-green">'+
                                        str_stock + stock[j].id + '</div>'+
                                        str_quantity + stock[j].sells + '</div>'+

                                        str_total + stock[j].id + '_pt>' + stock[j].price * stock[j].sells + '</div>'+
                                        str_single + stock[j].id + '_ps>' + stock[j].price + '</div>'+
                                        str_ch + stock[j].id + '_chshow>' + stock[j].chNet + '</div>'+
                                        sell_type_h +
                                        input_quantity + stock[j].sells + " value=" + stock[j].sells + " readonly id=" + stock[j].id + '>' +
                                        sell_div_self + stock[j].id + '>' +
                                        sell_type_t +
                                        input_price + stock[j].price + " id=" + stock[j].id +'_price>'+
                                        input_userid + userid + " id=" + stock[j].id + "__>"+
                                        '</div>';
                                    // console.log('reach');
                                }
                            }
                        if(real_stock != null) {
                            check.innerHTML = "";
                            for (let i = 0; i < real_stock.length; i++) {
                                amount[i] = parseInt(detail[i].quantity);
                                if (real_stock[i].chNet > 1)
                                    str_ch = str_chr;
                                else
                                    str_ch = str_chg;
                                elements.innerHTML +=
                                    '<div class="w3-panel">' +
                                    str_stock + detail[i].id + '</div>' +
                                    str_quantity + detail[i].quantity + '</div>' +

                                    str_total + real_stock[i].id + '_pt>' + real_stock[i].price * detail[i].quantity + '</div>' +
                                    str_single + real_stock[i].id + '_ps>' + real_stock[i].price + '</div>' +
                                    str_ch + real_stock[i].id + '_chshow>' + real_stock[i].chNet + '</div>' +
                                    sell_type_h +
                                    input_quantity + detail[i].quantity + " value=\"1\" id=" + detail[i].id + '>' +
                                    sell_div + detail[i].id + '>' +
                                    sell_type_t +
                                    input_price + real_stock[i].price + " id=" + detail[i].id + '_price>' +
                                    input_userid + userid + " id=" + detail[i].id + "__>" +
                                    '</div>'
                            }
                        }
                    });
                }
            });
            ///////////////////////////////////////////////////////////////////////////////////////

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a href=\"signin.html\" class=\"w3-bar-item w3-button\"> Login</a>";
            id.innerHTML = "<div align=\"right\">ID : None</div>";
            // document.getElementById('post_list').innerHTML = "";
        }
    });
    // selectram();
}

var random = setInterval(function () {
    renew();
}, 1000);

function getRandom(min,max){
    return Math.floor(Math.random()*(max-min+1))+min;
}

function renew() {
    var stockRef = firebase.database().ref('stock_list');
    stockRef.once('value').then( function (snapshot) {
        var obj = snapshot.val();
        var stock = Object.values(obj);
        var target = 0;
        for (let i = 0 ; i<stock.length ; i++ ) {
            var htmlpricet = document.getElementById(stock[i].id + "_pt");
            var htmlprices = document.getElementById(stock[i].id + "_ps");
            var htmlchNet = document.getElementById(stock[i].id + "_chshow");
            var _price = document.getElementById(stock[i].id+'_price');
            if(htmlpricet != null && htmlchNet != null && htmlprices != null) {

                var newNet = parseFloat(stock[i].chNet);

                if (newNet > 1)
                    htmlchNet.setAttribute('style', 'width:13%;' + ' color:red');
                else
                    htmlchNet.setAttribute('style', 'width:13%;' + ' color:green');
                if(real_stock != null){
                    for(let n = 0 ; n < real_stock.length ; n++){
                        // console.log(real_stock[n]);
                        if(real_stock[n].id == stock[i].id){
                            target = n;
                            break;
                        }
                    }
                }

                if(real_stock == null){
                    for (let i = 0 ; i<stock.length ; i++ ) {
                        if(stock[i].id == userid){
                            amount[0] = stock[i].sells;
                        }
                    }
                }
                htmlpricet.innerHTML = stock[i].price*amount[target];
                htmlprices.innerHTML = stock[i].price;
                htmlchNet.innerHTML = stock[i].chNet;
                _price.setAttribute('value', parseInt(stock[i].price));

            }
        }
        // console.log('renew');
    });
}

// function selectram() {
//     var stockRef = firebase.database().ref('stock_list');
//     stockRef.on('value',function (snapshot) {
//         setTimeout(function () {
//             var obj = snapshot.val();
//             var keys = Object.keys(obj);
//             var stock = Object.values(obj);
//             for (let i = 0 ; i<stock.length ; i++ ){
//                 // var htmlpricet = document.getElementById(stock[i].id + "_pt");
//                 // var htmlprices = document.getElementById(stock[i].id + "_ps");
//                 // var htmlchNet = document.getElementById(stock[i].id + "_chshow");
//                 // var _price = document.getElementById(stock[i].id+'_price');
//                 var oldprice = parseInt(stock[i].price);
//                 var newprice = oldprice + getRandom(minnum, maxnum);
//                 if (newprice < 0) {
//                     newprice = (-newprice) * 3;
//                 } else if (newprice > 1000) {
//                     newprice = newprice * 0.7329;
//                 }
//                 var newNet = (newprice / oldprice).toFixed(3);
//
//                 var index = i;
//                 var database = firebase.database();
//                 stock[index].price = newprice;
//                 stock[index].chNet = newNet;
//                 database.ref("stock_list/" + keys[index]).update(
//                     {
//                         id: stock[index].id,
//                         Fullname: stock[index].Fullname,
//                         sells: stock[index].sells,
//                         price: stock[index].price,
//                         chNet: stock[index].chNet
//                     }
//                 );
//             }
//         },10000);
//
//         });
// }

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        console.log("success");
        str_html = '<div class="w3-panel w3-pale-green w3-display-container w3-round-xxlarge">'+
            '<span onclick="this.parentElement.style.display=\'none\'"\n' +
            '  class="w3-button w3-round-xxlarge w3-right" >&times;</span>'+
            '<h3>Success!</h3>'+
            '<p>'+ message + '</p>'+
            '</div>';
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        console.log("error");
        str_html = '<div class="w3-panel w3-pale-red w3-display-container w3-round-xxlarge">'+
            '<span onclick="this.parentElement.style.display=\'none\'"\n' +
            '  class="w3-button w3-round-xxlarge w3-right" >&times;</span>'+
            '<h3>Error!</h3>'+
            '<p>'+ message + '</p>'+
            '</div>';
        alertarea.innerHTML = str_html;
    }
}


window.onload = function () {
    init();
};