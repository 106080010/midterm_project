
function ssort(a) {
    var elements = document.getElementById('merchandize');
    var stockRef = firebase.database().ref('stock_list');
    var str_stock = "<div class=\"stock w3-bar-item\" style=\"word-break : normal;width: 20%\">";
    var str_sells = "<div class=\"sells w3-bar-item\" style=\"width: 20%\">";
    var str_price = "<div class=\"price w3-bar-item\" style=\"width: 20%\" id=";
    var str_chr = "<div class=\"ch w3-bar-item\" style=\"width: 20%; color:red\" id=";
    var str_chg = "<div class=\"ch w3-bar-item\" style=\"width: 20%; color:green\" id=";
    var input_id = "<input class=\"w3-bar-item w3-border\" type=\"number\" min=\"1\" max=\"1000\" style=\"width: 80px\" value=\"1\" id=";
    var buy_div = "<div class=\"buy w3-bar-item w3-button w3-pale-green\" align=\"center\" style=\"width: 12%\" onclick=\"buything(id)\" id=";
    var input_sells = "<input type=\"hidden\" value=";
    var input_price = "<input type=\"hidden\" value=";
    var input_chNet = "<input type=\"hidden\" value=";
    var input_userid = "<input type=\"hidden\" value=";

    stockRef.once('value')
        .then(function (snapshot) {
            var obj = snapshot.val();
            var stock = Object.values(obj);

            if(a == 'p'){
                stock = stock.sort(function (a, b) {
                    return parseInt(a.price) < parseInt(b.price) ? 1 : -1;
                });
            } else if(a == 's'){
                stock = stock.sort(function (a, b) {
                    return parseInt(a.sells) < parseInt(b.sells) ? 1 : -1;
                });
            } else if(a == 'h'){
                stock = stock.sort(function (a, b) {
                    return a.chNet < b.chNet ? 1 : -1;
                });
            } else {
                stock = stock.sort(function (a, b) {
                    return a.chNet > b.chNet ? 1 : -1;
                });
            }
            var stock_id0 = stock[0].id;
            var str_ch;
            if(stock[0].chNet > 1)
                str_ch = str_chr;
            else
                str_ch = str_chg;
            elements.innerHTML =
                '<div class="w3-panel w3-animate-opacity">'+
                str_stock + stock[0].Fullname + '</div>'+
                str_sells + stock[0].sells + '</div>'+
                str_price + stock_id0 + '_pshow>' + stock[0].price + '</div>'+
                str_ch + stock_id0 + '_chshow>' + stock[0].chNet + '</div>'+
                input_id + stock_id0 + '>'+
                buy_div + stock_id0 + '>Buy</div>'+
                input_sells + stock[0].sells + " id=" + stock_id0 +'_sells>'+
                input_price + stock[0].price + " id=" + stock_id0 +'_price>'+
                input_chNet + stock[0].chNet + " id=" + stock_id0 +'_chNet>'+
                input_userid + userid + " id=" + stock_id0 + "__>"+
                '</div>';
            for (let i = 1 ; i<stock.length ; i++ ){
                var stock_id = stock[i].id;
                if(stock[i].chNet > 1)
                    str_ch = str_chr;
                else
                    str_ch = str_chg;
                elements.innerHTML +=
                    '<div class="w3-panel w3-animate-opacity">'+
                    str_stock + stock[i].Fullname + '</div>'+
                    str_sells + stock[i].sells + '</div>'+
                    str_price + stock_id + '_pshow>' + stock[i].price + '</div>'+
                    str_ch + stock_id + '_chshow>' + stock[i].chNet  + '</div>'+
                    input_id + stock_id + '>'+
                    buy_div + stock_id + '>Buy</div>'+
                    input_sells + stock[i].sells + " id=" + stock_id +'_sells>'+
                    input_price + stock[i].price + " id=" + stock_id +'_price>'+
                    input_chNet + stock[i].chNet + " id=" + stock_id +'_chNet>'+
                    input_userid + userid + " id=" + stock_id + "__>"+
                    '</div>';
            }

        });
}



function alignment(a,id) {
    ssort(a);
}